# Assignment 1 - Agile Software Practice.

__Name:__ Yuanhao Luo

This repository contains the implementation of a React App and its associated Cypress tests and GitLab CI pipeline.

## React App Features.
 
+ An extra filter
+ A new parameterised URL(Production Company)
+ Pagination
+ Firebase authentication
+ Full caching support
+ New Material UI components
+ Create a generic detail page so that I can easily add detail pages not only movie and company detail page
+ Code splitting
+ Continuous Integration

## Automated Tests.

### Best test cases.

+ cypress/e2e/authentication.cy.js
+ cypress/e2e/review.cy.js

### Cypress Custom commands (if relevant).

+ movies/cypress/e2e/authentication.cy.js
+ movies/cypress/e2e/upcoming.cy.js
+ movies/cypress/e2e/review.cy.js

## Code Splitting.

+ movies/src/index.js
+ src/components/movieDetails/index.js
+ src/components/siteHeader/index.js

## Pull Requests.

https://github.com/Yuanhao-Luo/react-movie-assignment

## Independent learning (If relevant).

Use GitHub Pages to complete auto-deployment. The url of page is determined in package.json
![](./images/Auto-deployment.jpg)

GitLab Repository: https://gitlab.com/lyh1097308076/moviesapp-agile.git
I have run out of my pipeline time, so there are many fails in gitlab ci.